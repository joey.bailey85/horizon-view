terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "2.2.0"
    }
  }
}

provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = var.datacenter
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network1" {
  name          = var.network_name1
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network2" {
  name          = var.network_name2
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network3" {
  name          = var.network_name3
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_host" "host" {
  name          = var.deployment_host
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "resource_pool" {
  name          = format("%s%s", data.vsphere_compute_cluster.cluster.name, "/Resources")
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_ovf_vm_template" "uag_local" {
  name             = "UAG-Deploy"
  resource_pool_id = data.vsphere_resource_pool.resource_pool.id
  datastore_id     = data.vsphere_datastore.datastore.id
  host_system_id   = data.vsphere_host.host.id
  local_ovf_path   = var.local_ovf_path
  ovf_network_map = {
    "Network 1" = data.vsphere_network.network1.id
    "Network 2" = data.vsphere_network.network2.id
    "Network 3" = data.vsphere_network.network3.id
  }
}

resource "vsphere_virtual_machine" "uag" {
  count                = var.uag_number
  name                 = "${var.uag_name_prefix}${count.index + 1}"
  datacenter_id        = data.vsphere_datacenter.dc.id
  datastore_id         = data.vsphere_ovf_vm_template.uag_local.datastore_id
  resource_pool_id     = data.vsphere_ovf_vm_template.uag_local.resource_pool_id
  num_cpus             = data.vsphere_ovf_vm_template.uag_local.num_cpus
  num_cores_per_socket = data.vsphere_ovf_vm_template.uag_local.num_cores_per_socket
  memory               = data.vsphere_ovf_vm_template.uag_local.memory
  guest_id             = data.vsphere_ovf_vm_template.uag_local.guest_id
  host_system_id       = data.vsphere_host.host.id
  folder               = var.horizon_folder
  scsi_type            = data.vsphere_ovf_vm_template.uag_local.scsi_type
  dynamic "network_interface" {
    for_each = data.vsphere_ovf_vm_template.uag_local.ovf_network_map
    content {
      network_id = network_interface.value
    }
  }
  wait_for_guest_net_timeout = 5

  ovf_deploy {
    allow_unverified_ssl_cert = true
    local_ovf_path            = var.local_ovf_path
    disk_provisioning         = var.disk_provisioning
    deployment_option         = var.deployment_option
  }

  vapp {
    properties = {
      "uagName"        = "${var.uag_name_prefix}${count.index + 1}"
      "ipMode0"        = var.ipMode0
      "ipMode1"        = var.ipMode1
      "ipMode2"        = var.ipMode2
      "DNS"            = var.DNS
      "dnsSearch"      = var.dnsSearch
      "ip0"            = "${var.uag_prefix_ip0}${var.uag_start_address0 + count.index}"
      "ip1"            = "${var.uag_prefix_ip1}${var.uag_start_address1 + count.index}"
      "ip2"            = "${var.uag_prefix_ip2}${var.uag_start_address2 + count.index}"
      "netmask0"       = var.netmask0
      "netmask1"       = var.netmask1
      "netmask2"       = var.netmask2
      "routes1"        = var.routes1
      "routes2"        = var.routes2
      "defaultGateway" = var.defaultGateway
      "adminPassword"  = var.adminPassword
      "rootPassword"   = var.rootPassword
    }
  }
  lifecycle {
    ignore_changes = [
      host_system_id,
      vapp[0].properties["adminPassword"],
      vapp[0].properties["rootPassword"]
    ]
  }
}
