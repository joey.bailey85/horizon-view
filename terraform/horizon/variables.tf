variable "vsphere_server" {
  description = "vCenter Server FQDN"
  type        = string
}

variable "vsphere_user" {
  description = "vSphere Username"
  type        = string
}

variable "vsphere_password" {
  description = "vSphere Password"
  type        = string
  sensitive   = true
}

variable "datacenter" {
  description = "vSphere Datacenter"
  type        = string
}

variable "cluster" {
  description = "vSphere Cluster"
  type        = string
}

variable "datastore" {
  description = "vSphere Datastore"
  type        = string
}

variable "network_name" {
  description = "VM Port Group"
  type        = string
}

variable "windows_name" {
  description = "Windows Template Name"
  type        = string
}

variable "windows_domain" {
  description = "Windows Domain FQDN"
  type        = string
}

variable "windows_user" {
  description = "Windows Domain User"
  type        = string
}

variable "windows_password" {
  description = "Windows Domain Password"
  type        = string
  sensitive   = true
}

variable "cpus" {
  description = "Number of CPUs"
  type        = number
}

variable "ram" {
  description = "Amount of RAM in MB"
  type        = number
}

variable "disk_label" {
  description = "Disk Label"
  type        = string
}

variable "thin_provisioned" {
  description = "Is disk thin provisioned"
  type        = bool
}

variable "disk_size" {
  description = "Disk size in GB"
  type        = number
}

variable "guest_os_id" {
  description = "Guest OS ID"
  type        = string
}

variable "firmware" {
  description = "Firmware"
  type        = string
}

variable "scsi_type" {
  description = "VMDK SCSI Type"
  type        = string
}

variable "timezone" {
  description = "Timezone code: https://learn.microsoft.com/en-us/previous-versions/windows/embedded/ms912391(v=winembedded.11)?redirectedfrom=MSDN"
  type        = number
}

variable "netmask" {
  description = "Netmask, ex. 24"
  type        = number
}

variable "gateway" {
  description = "Gateway IP"
  type        = string
}

variable "dns_server" {
  description = "DNS Server IP"
  type        = string
}

variable "virtual_machine_name_prefix" {
  description = "Name Prefix for Horizon VMs"
  type        = string
}

variable "server_number" {
  description = "Number of Connection Server VMs to Deploy"
  type        = string
}

variable "template_password" {
  description = "Local Administrator Password for Cloning Template"
  type        = string
  sensitive   = true
}

variable "prefix_ip" {
  description = "Starting IP address prefix for Connection Brokers"
  type        = string
}

variable "starting_address" {
  description = "Number of Connection Server VMs to Deploy"
  type        = number
}

variable "appvolumes_name" {
  description = "Name for AppVolumes VM"
  type        = string
}

variable "appvolumes_ip" {
  description = "AppVolumes VM IP"
  type        = string
}

variable "horizon_folder" {
  description = "Folder name for Horizon VMs"
  type        = string
}