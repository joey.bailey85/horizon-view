terraform {
  required_providers {
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "2.2.0"
    }
  }
}

provider "vsphere" {
  user                 = var.vsphere_user
  password             = var.vsphere_password
  vsphere_server       = var.vsphere_server
  allow_unverified_ssl = true
}

data "vsphere_datacenter" "dc" {
  name = var.datacenter
}

data "vsphere_compute_cluster" "cluster" {
  name          = var.cluster
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "datastore" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = var.network_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "windows" {
  name          = "/${var.datacenter}/vm/${var.windows_name}"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "resource_pool" {
  name          = format("%s%s", data.vsphere_compute_cluster.cluster.name, "/Resources")
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_folder" "horizon" {
  path          = var.horizon_folder
  type          = "vm"
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "horizon" {
  count            = var.server_number
  name             = "${var.virtual_machine_name_prefix}${count.index + 1}"
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  folder           = var.horizon_folder
  depends_on = [
    vsphere_folder.horizon
  ]

  num_cpus = var.cpus
  memory   = var.ram

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  wait_for_guest_net_timeout = -1
  wait_for_guest_ip_timeout  = -1

  disk {
    label            = var.disk_label
    thin_provisioned = var.thin_provisioned
    size             = var.disk_size
  }
  guest_id  = var.guest_os_id
  firmware  = var.firmware
  scsi_type = var.scsi_type

  clone {
    template_uuid = data.vsphere_virtual_machine.windows.id
    customize {
      network_interface {
        ipv4_address = "${var.prefix_ip}${var.starting_address + count.index}"
        ipv4_netmask = var.netmask
        dns_server_list = [
          var.dns_server
        ]
      }
      windows_options {
        computer_name         = "${var.virtual_machine_name_prefix}${count.index + 1}"
        join_domain           = var.windows_domain
        domain_admin_user     = var.windows_user
        domain_admin_password = var.windows_password
        time_zone             = var.timezone
        admin_password        = var.template_password
      }
      ipv4_gateway = var.gateway
    }
  }
}

resource "vsphere_virtual_machine" "appvolumes" {
  name             = var.appvolumes_name
  resource_pool_id = data.vsphere_compute_cluster.cluster.resource_pool_id
  datastore_id     = data.vsphere_datastore.datastore.id
  folder           = var.horizon_folder
  depends_on = [
    vsphere_folder.horizon
  ]

  num_cpus = var.cpus
  memory   = var.ram

  network_interface {
    network_id = data.vsphere_network.network.id
  }

  wait_for_guest_net_timeout = -1
  wait_for_guest_ip_timeout  = -1

  disk {
    label            = var.disk_label
    thin_provisioned = var.thin_provisioned
    size             = var.disk_size
  }
  guest_id  = var.guest_os_id
  firmware  = var.firmware
  scsi_type = var.scsi_type

  clone {
    template_uuid = data.vsphere_virtual_machine.windows.id
    customize {
      network_interface {
        ipv4_address = var.appvolumes_ip
        ipv4_netmask = var.netmask
        dns_server_list = [
          var.dns_server
        ]
      }
      windows_options {
        computer_name         = var.appvolumes_name
        join_domain           = var.windows_domain
        domain_admin_user     = var.windows_user
        domain_admin_password = var.windows_password
        time_zone             = var.timezone
        admin_password        = var.template_password
      }
      ipv4_gateway = var.gateway
    }
  }
}